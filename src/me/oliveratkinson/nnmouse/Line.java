package me.oliveratkinson.nnmouse;


import java.awt.*;
import java.util.ArrayList;

/**
 * Copyright 2013 Oliver Atkinson
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Line {

    private Point start, end;
    public ArrayList<PointTime> pointTimeList = new ArrayList<PointTime>();

    public Line(Point start) {
        this.start = start;
    }

    private Line(ArrayList<PointTime> pointTimeList) throws IllegalArgumentException{
        if (pointTimeList.size() == 0) {
            throw new IllegalArgumentException("List cannot have no entries");
        }
        this.start = pointTimeList.get(0).point();
        this.end = pointTimeList.get(pointTimeList.size() - 1).point();
        this.pointTimeList = pointTimeList;
    }

    public void addPoint(Point point) {
        this.pointTimeList.add(new PointTime(point));
    }

    public void finish() {
        this.end = this.pointTimeList.get(this.pointTimeList.size() - 1).point();
    }

    public double gradient() {
        return dy() / dx();
    }

    protected int dx() {
         return this.start.x - this.end.x;
    }

    protected int dy() {
        return this.start.y - this.end.y;
    }

    public double absoluteDistance() {
        double distance = 0D;
        PointTime previous = null;
        for (PointTime pointTime : this.pointTimeList) {
            if (previous != null) {
                distance += previous.distanceTo(pointTime);
            }
            previous = pointTime;
        }
        return distance;
    }

    public double distance() {
        if (this.end == null) {
            return 0D;
        }
        return Math.sqrt(Math.pow((this.end.getX() - this.start.getX()), 2)
                + Math.pow((this.end.getY() - this.start.getY()), 2));
    }

    public long timeTaken() {
        return this.pointTimeList.get(this.pointTimeList.size() - 1).time() - this.pointTimeList.get(0).time();
    }

    public Point midpoint() {
        return new Point(((this.end.x - this.start.x) / 2) + this.start.x,
                ((this.end.y - this.start.y) / 2) + this.start.y);
    }

    //rotation using a translation matrix
    public Line rotate(int degrees) {
        double theta = Math.toRadians(degrees);
        Point midpoint = this.midpoint();
        ArrayList<PointTime> rotated = new ArrayList<PointTime>();

        for (PointTime pointTime : this.pointTimeList) {
            Point point = pointTime.point();
            double px = midpoint.x - ((point.x - midpoint.x) * Math.cos(theta)) - ((point.y - midpoint.y) * Math.sin(theta));
            double py = midpoint.y - ((point.x - midpoint.x) * Math.sin(theta)) + ((point.y - midpoint.y) * Math.cos(theta));
            PointTime rotatedPointTime = new PointTime(new Point((int)px, (int)py), pointTime.time());
            rotated.add(rotatedPointTime);
        }
        return new Line(rotated);
    }
}

class PointTime {

    private Point point;
    private long time;

    public PointTime(Point point) {
        this(point, System.currentTimeMillis());
    }

    public PointTime(Point point, long time) {
        this.point = point;
        this.time = time;
    }

    public Point point() {
        return this.point;
    }

    public long time() {
        return this.time;
    }

    // sqrt(a^2 + b^2);
    public double distanceTo(PointTime other) {
        return Math.sqrt(Math.pow((other.point().getX() - point().getX()), 2)
                + Math.pow((other.point().getY() - point().getY()), 2));
    }

    public double speed(PointTime other) {
        return distanceTo(other) / (time - other.time());
    }
}