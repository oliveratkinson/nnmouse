package me.oliveratkinson.nnmouse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Copyright 2013 Oliver Atkinson
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MouseRecorder extends JFrame {

    private boolean recording = false;
    private Line line;

    public MouseRecorder() {
        super("Mouse recorder");
        super.setSize(600, 600);
        final CustomComponent component = new CustomComponent();
        super.add(component);

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                recording = !recording;
                if (recording) {
                    System.out.println("Recording...");
                    line = new Line(mouseEvent.getPoint());
                } else {
                    System.out.println("Finished!");
                    line.finish();
                    component.line = line;
                    component.degrees = 0;
                    component.repaint();
                }
            }

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                if (recording) {
                    line.addPoint(mouseEvent.getPoint());
                }
            }
        };

        component.addMouseMotionListener(mouseAdapter);
        component.addMouseListener(mouseAdapter);
    }
}

class CustomComponent extends JComponent {

    public Line line;

    private static final long serialVersionUID = 1L;

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(600, 600);
    }

    @Override
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }

    int degrees = 0;
    @Override
    public void paintComponent(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, 600, 600);
        if (this.line != null) {
            drawLine(this.line, g);
            drawLine(this.line.rotate(degrees++), g);
            try { Thread.sleep(50); } catch (InterruptedException e) { }
        }
        super.paintComponent(g);
        repaint();
    }

    void drawLine(Line line, Graphics g) {
        PointTime previous = null;
        double maximum = 0D;
        for (PointTime pointTime : line.pointTimeList) {
            if (previous != null) {
                double speed = Math.abs(previous.speed(pointTime));
                Point start = previous.point(), end = pointTime.point();
                int r = (int) Math.max(0, Math.min(255, (255 / maximum) * speed));
                g.setColor(new Color(r, 255 - r, 0));
                g.drawLine(start.x, start.y, end.x, end.y);
                maximum = Math.max(speed, maximum);
            }
            previous = pointTime;
        }
    }
}